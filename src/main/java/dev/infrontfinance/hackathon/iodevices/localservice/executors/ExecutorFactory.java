package dev.infrontfinance.hackathon.iodevices.localservice.executors;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Action.ActionTypeEnum;
import dev.infrontfinance.hackathon.iodevices.localservice.exception.LocalServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ExecutorFactory {

  private final KeyExecutor keyExecutor;

  public AbstractExecutor getExecutor(ActionTypeEnum actionType) {
    if (actionType == ActionTypeEnum.SENDKEY) {
      return keyExecutor;
    } else {
      throw new LocalServiceException(
          "ExecutorFactory: no executor class defined for " + actionType);
    }
  }
}
