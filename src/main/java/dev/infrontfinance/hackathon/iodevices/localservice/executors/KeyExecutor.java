package dev.infrontfinance.hackathon.iodevices.localservice.executors;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Action;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class KeyExecutor implements AbstractExecutor {
  private final Keyboard keyboard;

  @Override
  public void execute(Action action) {
    try {
      keyboard.switchTo(action.getTarget());
      if (action.getValue() != null) {
        keyboard.press(action.getValue());
      }
    } catch (Exception e) {
      log.error("switch to " + action.getTarget() + " failed!", e);
    }
  }
}
