package dev.infrontfinance.hackathon.iodevices.localservice.executors;

import static java.awt.event.KeyEvent.VK_0;
import static java.awt.event.KeyEvent.VK_1;
import static java.awt.event.KeyEvent.VK_2;
import static java.awt.event.KeyEvent.VK_3;
import static java.awt.event.KeyEvent.VK_4;
import static java.awt.event.KeyEvent.VK_5;
import static java.awt.event.KeyEvent.VK_6;
import static java.awt.event.KeyEvent.VK_7;
import static java.awt.event.KeyEvent.VK_8;
import static java.awt.event.KeyEvent.VK_9;
import static java.awt.event.KeyEvent.VK_A;
import static java.awt.event.KeyEvent.VK_ALT;
import static java.awt.event.KeyEvent.VK_ALT_GRAPH;
import static java.awt.event.KeyEvent.VK_AMPERSAND;
import static java.awt.event.KeyEvent.VK_B;
import static java.awt.event.KeyEvent.VK_C;
import static java.awt.event.KeyEvent.VK_CIRCUMFLEX;
import static java.awt.event.KeyEvent.VK_COMMA;
import static java.awt.event.KeyEvent.VK_CONTROL;
import static java.awt.event.KeyEvent.VK_D;
import static java.awt.event.KeyEvent.VK_E;
import static java.awt.event.KeyEvent.VK_ENTER;
import static java.awt.event.KeyEvent.VK_F;
import static java.awt.event.KeyEvent.VK_G;
import static java.awt.event.KeyEvent.VK_H;
import static java.awt.event.KeyEvent.VK_I;
import static java.awt.event.KeyEvent.VK_J;
import static java.awt.event.KeyEvent.VK_K;
import static java.awt.event.KeyEvent.VK_L;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_MINUS;
import static java.awt.event.KeyEvent.VK_N;
import static java.awt.event.KeyEvent.VK_NUMBER_SIGN;
import static java.awt.event.KeyEvent.VK_O;
import static java.awt.event.KeyEvent.VK_P;
import static java.awt.event.KeyEvent.VK_PERIOD;
import static java.awt.event.KeyEvent.VK_PLUS;
import static java.awt.event.KeyEvent.VK_Q;
import static java.awt.event.KeyEvent.VK_R;
import static java.awt.event.KeyEvent.VK_S;
import static java.awt.event.KeyEvent.VK_SHIFT;
import static java.awt.event.KeyEvent.VK_SPACE;
import static java.awt.event.KeyEvent.VK_T;
import static java.awt.event.KeyEvent.VK_TAB;
import static java.awt.event.KeyEvent.VK_U;
import static java.awt.event.KeyEvent.VK_V;
import static java.awt.event.KeyEvent.VK_W;
import static java.awt.event.KeyEvent.VK_X;
import static java.awt.event.KeyEvent.VK_Y;
import static java.awt.event.KeyEvent.VK_Z;

import com.sun.jna.platform.DesktopWindow;
import dev.infrontfinance.hackathon.iodevices.localservice.exception.LocalServiceException;
import dev.infrontfinance.hackathon.iodevices.localservice.helper.ProcessManager;
import dev.infrontfinance.hackathon.iodevices.localservice.helper.TaskManager;
import java.awt.Robot;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class Keyboard {

  private final Robot robot;

  public void press(String keys) {
    StringBuilder buffer = new StringBuilder();
    for (int i = 0; i < keys.length(); i++) {
      char character = keys.charAt(i);
      if (('<' == character) || ('>' == character)) {
        buffer.append(character);
      } else {
        typeString(buffer.toString() + character);
        buffer = new StringBuilder();
        try {
          if ('\n' == character) {
            Thread.sleep(2000);
          } else {
            Thread.sleep(100);
          }
        } catch (InterruptedException e) {
          log.warn("Interrupted!", e);
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  public void typeString(String characters) {
    List<Integer> keyCodes = new ArrayList<>();
    for (int i = 0; i < characters.length(); i++) {
      keyCodes.addAll(getCodes(characters.charAt(i)));
    }
    doType(keyCodes.stream().mapToInt(i -> i).toArray());
  }

  @SuppressWarnings("java:S1479")
  private List<Integer> getCodes(char character) {
    List<Integer> keyList = new ArrayList<>();
    switch (character) {
      case 'a':
        keyList.add(VK_A);
        break;
      case 'b':
        keyList.add(VK_B);
        break;
      case 'c':
        keyList.add(VK_C);
        break;
      case 'd':
        keyList.add(VK_D);
        break;
      case 'e':
        keyList.add(VK_E);
        break;
      case 'f':
        keyList.add(VK_F);
        break;
      case 'g':
        keyList.add(VK_G);
        break;
      case 'h':
        keyList.add(VK_H);
        break;
      case 'i':
        keyList.add(VK_I);
        break;
      case 'j':
        keyList.add(VK_J);
        break;
      case 'k':
        keyList.add(VK_K);
        break;
      case 'l':
        keyList.add(VK_L);
        break;
      case 'm':
        keyList.add(VK_M);
        break;
      case 'n':
        keyList.add(VK_N);
        break;
      case 'o':
        keyList.add(VK_O);
        break;
      case 'p':
        keyList.add(VK_P);
        break;
      case 'q':
        keyList.add(VK_Q);
        break;
      case 'r':
        keyList.add(VK_R);
        break;
      case 's':
        keyList.add(VK_S);
        break;
      case 't':
        keyList.add(VK_T);
        break;
      case 'u':
        keyList.add(VK_U);
        break;
      case 'v':
        keyList.add(VK_V);
        break;
      case 'w':
        keyList.add(VK_W);
        break;
      case 'x':
        keyList.add(VK_X);
        break;
      case 'y':
        keyList.add(VK_Y);
        break;
      case 'z':
        keyList.add(VK_Z);
        break;
      case 'A':
        keyList.add(VK_SHIFT);
        keyList.add(VK_A);
        break;
      case 'B':
        keyList.add(VK_SHIFT);
        keyList.add(VK_B);
        break;
      case 'C':
        keyList.add(VK_SHIFT);
        keyList.add(VK_C);
        break;
      case 'D':
        keyList.add(VK_SHIFT);
        keyList.add(VK_D);
        break;
      case 'E':
        keyList.add(VK_SHIFT);
        keyList.add(VK_E);
        break;
      case 'F':
        keyList.add(VK_SHIFT);
        keyList.add(VK_F);
        break;
      case 'G':
        keyList.add(VK_SHIFT);
        keyList.add(VK_G);
        break;
      case 'H':
        keyList.add(VK_SHIFT);
        keyList.add(VK_H);
        break;
      case 'I':
        keyList.add(VK_SHIFT);
        keyList.add(VK_I);
        break;
      case 'J':
        keyList.add(VK_SHIFT);
        keyList.add(VK_J);
        break;
      case 'K':
        keyList.add(VK_SHIFT);
        keyList.add(VK_K);
        break;
      case 'L':
        keyList.add(VK_SHIFT);
        keyList.add(VK_L);
        break;
      case 'M':
        keyList.add(VK_SHIFT);
        keyList.add(VK_M);
        break;
      case 'N':
        keyList.add(VK_SHIFT);
        keyList.add(VK_N);
        break;
      case 'O':
        keyList.add(VK_SHIFT);
        keyList.add(VK_O);
        break;
      case 'P':
        keyList.add(VK_SHIFT);
        keyList.add(VK_P);
        break;
      case 'Q':
        keyList.add(VK_SHIFT);
        keyList.add(VK_Q);
        break;
      case 'R':
        keyList.add(VK_SHIFT);
        keyList.add(VK_R);
        break;
      case 'S':
        keyList.add(VK_SHIFT);
        keyList.add(VK_S);
        break;
      case 'T':
        keyList.add(VK_SHIFT);
        keyList.add(VK_T);
        break;
      case 'U':
        keyList.add(VK_SHIFT);
        keyList.add(VK_U);
        break;
      case 'V':
        keyList.add(VK_SHIFT);
        keyList.add(VK_V);
        break;
      case 'W':
        keyList.add(VK_SHIFT);
        keyList.add(VK_W);
        break;
      case 'X':
        keyList.add(VK_SHIFT);
        keyList.add(VK_X);
        break;
      case 'Y':
        keyList.add(VK_SHIFT);
        keyList.add(VK_Y);
        break;
      case 'Z':
        keyList.add(VK_SHIFT);
        keyList.add(VK_Z);
        break;
      case '0':
        keyList.add(VK_0);
        break;
      case '1':
        keyList.add(VK_1);
        break;
      case '2':
        keyList.add(VK_2);
        break;
      case '3':
        keyList.add(VK_3);
        break;
      case '4':
        keyList.add(VK_4);
        break;
      case '5':
        keyList.add(VK_5);
        break;
      case '6':
        keyList.add(VK_6);
        break;
      case '7':
        keyList.add(VK_7);
        break;
      case '8':
        keyList.add(VK_8);
        break;
      case '9':
        keyList.add(VK_9);
        break;
      case '-':
        keyList.add(VK_MINUS);
        break;
      case '=':
        keyList.add(VK_SHIFT);
        keyList.add(VK_0);
        break;
      case '~':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_PLUS);
        break;
      case '!':
        keyList.add(VK_SHIFT);
        keyList.add(VK_1);
        break;
      case '@':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_Q);
        break;
      case '#':
        keyList.add(VK_NUMBER_SIGN);
        break;
      case '$':
        keyList.add(VK_SHIFT);
        keyList.add(VK_4);
        break;
      case '%':
        keyList.add(VK_SHIFT);
        keyList.add(VK_5);
        break;
      case '^':
        keyList.add(VK_CIRCUMFLEX);
        break;
      case '&':
        keyList.add(VK_SHIFT);
        keyList.add(VK_6);
        break;
      case '*':
        keyList.add(VK_SHIFT);
        keyList.add(VK_PLUS);
        break;
      case '(':
        keyList.add(VK_SHIFT);
        keyList.add(VK_8);
        break;
      case ')':
        keyList.add(VK_SHIFT);
        keyList.add(VK_9);
        break;
      case '_':
        keyList.add(VK_SHIFT);
        keyList.add(VK_MINUS);
        break;
      case '+':
        keyList.add(VK_PLUS);
        break;
      case '\t':
        keyList.add(VK_TAB);
        break;
      case '\n':
        keyList.add(VK_ENTER);
        break;
      case '[':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_8);
        break;
      case ']':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_9);
        break;
      case '\\':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_AMPERSAND);
        break;
      case '{':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_7);
        break;
      case '}':
        keyList.add(VK_ALT_GRAPH);
        keyList.add(VK_0);
        break;
      case ';':
        keyList.add(VK_SHIFT);
        keyList.add(VK_COMMA);
        break;
      case ':':
        keyList.add(VK_SHIFT);
        keyList.add(VK_PERIOD);
        break;
      case '\'':
        keyList.add(VK_SHIFT);
        doType(VK_NUMBER_SIGN);
        break;
      case '"':
        keyList.add(VK_SHIFT);
        keyList.add(VK_2);
        break;
      case ',':
        keyList.add(VK_COMMA);
        break;
      case '.':
        keyList.add(VK_PERIOD);
        break;
      case '/':
        keyList.add(VK_SHIFT);
        keyList.add(VK_7);
        break;
      case '?':
        keyList.add(VK_SHIFT);
        keyList.add(VK_AMPERSAND);
        break;
      case ' ':
        keyList.add(VK_SPACE);
        break;
      case '<':
        keyList.add(VK_CONTROL);
        break;
      case '>':
        keyList.add(VK_ALT);
        break;
      default:
        throw new IllegalArgumentException("Cannot type character: " + character);
    }
    return keyList;
  }

  private void doType(int... keyCodes) {
    doType(keyCodes, 0, keyCodes.length);
  }

  private void doType(int[] keyCodes, int offset, int length) {
    if (length == 0) {
      return;
    }
    robot.keyPress(keyCodes[offset]);
    doType(keyCodes, offset + 1, length - 1);
    robot.keyRelease(keyCodes[offset]);
  }

  public void switchTo(String name) throws Exception {
    if (!ProcessManager.isProcessRunning(name)) {
      startProcess(name);
    }
    if (ProcessManager.isProcessRunning(name)) {
      int processId = ProcessManager.getProcessId(name);
      Optional<DesktopWindow> processWindow = ProcessManager.getProcessWindow(processId);
      if ((!processWindow.isPresent()) || (!TaskManager.switchToProcess(processWindow.get()))) {
        throw new LocalServiceException("Could not switch to process " + name);
      }
    } else {
      throw new LocalServiceException("Process not started: " + name);
    }
  }

  private void startProcess(String target) throws IOException, InterruptedException {
    if ("notepad++.exe".equals(target)) {
      new ProcessBuilder("C:\\\\Program Files (x86)\\\\Notepad++\\\\notepad++.exe").start();
      Thread.sleep(1000);
    } else if ("vwdpm.exe".equals(target)) {
      new ProcessBuilder("C:\\vwd\\6.DE\\bin\\vwdpm.exe").start();
      Thread.sleep(10000);
      press("DEMO\n");
      Thread.sleep(13000);
      press("\t\n");
    }
  }
}
