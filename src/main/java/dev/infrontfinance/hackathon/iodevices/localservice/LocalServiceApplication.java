package dev.infrontfinance.hackathon.iodevices.localservice;

import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

@SpringBootApplication(scanBasePackages = {"dev.infrontfinance"})
@RequiredArgsConstructor
//@SuppressWarnings({"squid:S1118"})
@Slf4j
public class LocalServiceApplication extends SpringBootServletInitializer {

  private final Environment env;

  //@SuppressWarnings({"squid:S2095", "squid:S1313", "squid:S2629"})
  public static void main(String[] args) throws UnknownHostException {


    SpringApplication app = new SpringApplication(LocalServiceApplication.class);

    Environment env = app.run(args).getEnvironment();
    String protocol = "http";
    if (env.getProperty("server.ssl.key-store") != null) {
      protocol = "https";
    }

    String serverPort = env.getProperty("server.port");

    log.info(
        "\n----------------------------------------------------------"
            + "\n\tApplication '{}' is running! Access URLs:"
            + "\n\tLocal           : \t{}://localhost:{}"
            + "\n\tExternal        : \t{}://{}:{}"
            + "\n\tProfile(s)      : \t{}"
            + "\n----------------------------------------------------------",
        env.getProperty("spring.application.name"),
        protocol,
        serverPort,
        protocol,
        InetAddress.getLocalHost().getHostAddress(),
        serverPort,
        env.getActiveProfiles());
  }

  static {
    System.setProperty("java.awt.headless", "false");
  }

}
