package dev.infrontfinance.hackathon.iodevices.localservice.helper;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.DesktopWindow;
import com.sun.jna.platform.WindowUtils;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.ptr.PointerByReference;
import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import org.apache.logging.log4j.util.Strings;

@UtilityClass
public class TaskManager {

  private static final int MAX_TITLE_LENGTH = 1024;

  public static String getActiveWindowProcess() {
    char[] buffer = new char[MAX_TITLE_LENGTH * 2];
    PointerByReference pointer = new PointerByReference();
    HWND foregroundWindow = User32DLL.GetForegroundWindow();
    User32DLL.GetWindowThreadProcessId(foregroundWindow, pointer);
    Pointer process =
        Kernel32.OpenProcess(
            Kernel32.PROCESS_QUERY_INFORMATION | Kernel32.PROCESS_VM_READ,
            false,
            pointer.getValue());
    Psapi.GetModuleBaseNameW(process, null, buffer, MAX_TITLE_LENGTH);
    return Native.toString(buffer);
  }

  public static List<DesktopWindow> getVisibleWindows(List<HWND> processList) {
    List<DesktopWindow> allWindows = WindowUtils.getAllWindows(false);
    List<DesktopWindow> processWindows =
        allWindows.stream()
            .filter(desktopWindow -> processList.contains(desktopWindow.getHWND()))
            .collect(Collectors.toList());
    List<DesktopWindow> titledWindows =
        processWindows.stream()
            .filter(desktopWindow -> Strings.isNotBlank(desktopWindow.getTitle()))
            .collect(Collectors.toList());
    return titledWindows.stream()
        .filter(desktopWindow -> isBig(desktopWindow) || isFancy(desktopWindow))
        .collect(Collectors.toList());
  }

  private static boolean isFancy(DesktopWindow desktopWindow) {
    return (desktopWindow.getTitle().equals("Infront"))
        || ((0 < desktopWindow.getLocAndSize().getWidth())
            && (0 < desktopWindow.getLocAndSize().getHeight())
            && (-30000 > desktopWindow.getLocAndSize().getX())
            && (-30000 > desktopWindow.getLocAndSize().getY()));
  }

  private static boolean isBig(DesktopWindow desktopWindow) {
    return (899 < desktopWindow.getLocAndSize().getWidth())
        && (599 < desktopWindow.getLocAndSize().getHeight());
  }

  public static boolean switchToProcess(DesktopWindow process) {
    HWND windowsHwnd = User32DLL.FindWindowA(null, process.getTitle());
    User32DLL.ShowWindow(windowsHwnd, WinUser.SW_SHOWMAXIMIZED);
    boolean done = User32DLL.SetForegroundWindow(windowsHwnd);
    User32DLL.SetFocus(User32DLL.GetForegroundWindow());
    return done;
  }

  @SuppressWarnings({"java:S1118", "java:S100"}) // native access
  static class Psapi {
    static {
      Native.register("psapi");
    }

    public static native int GetModuleBaseNameW(
        Pointer hProcess, Pointer hmodule, char[] lpBaseName, int size);
  }

  @SuppressWarnings({
    "java:S1118",
    "java:S100",
    "java:S1104",
    "java:S1444",
    "java:S3008"
  }) // native access
  static class Kernel32 {
    public static int PROCESS_QUERY_INFORMATION = 0x0400;
    public static int PROCESS_VM_READ = 0x0010;

    static {
      Native.register("kernel32");
    }

    public static native Pointer OpenProcess(
        int dwDesiredAccess, boolean bInheritHandle, Pointer pointer);
  }

  @SuppressWarnings({"java:S1118", "java:S100"}) // native access
  static class User32DLL {
    static {
      Native.register("user32");
    }

    public static native int GetWindowThreadProcessId(HWND hWnd, PointerByReference pref);

    public static native HWND GetForegroundWindow();

    public static native boolean SetForegroundWindow(HWND hWnd);

    public static native HWND FindWindowA(String lpClassName, String lpWindowName);

    public static native HWND SetFocus(HWND hWnd);

    public static native boolean ShowWindow(HWND hWnd, int nCmdShow);
  }
}
