package dev.infrontfinance.hackathon.iodevices.localservice.controller;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Action;
import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import dev.infrontfinance.hackathon.iodevices.localservice.clients.ConfigServiceClient;
import dev.infrontfinance.hackathon.iodevices.localservice.executors.AbstractExecutor;
import dev.infrontfinance.hackathon.iodevices.localservice.executors.ExecutorFactory;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class LocalServiceController {

  private final ConfigServiceClient configServiceClient;
  private final ExecutorFactory executorFactory;

  public void executeConfig(UUID configID) {
    Config config = this.configServiceClient.getConfig(configID);
    if (config.getActions() != null) {
      config.getActions().forEach(this::executeAction);
    }
  }

  private void executeAction(Action action) {
    AbstractExecutor executor = executorFactory.getExecutor(action.getActionType());
    executor.execute(action);
  }
}
