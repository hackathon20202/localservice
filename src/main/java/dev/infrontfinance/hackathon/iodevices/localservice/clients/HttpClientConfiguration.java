package dev.infrontfinance.hackathon.iodevices.localservice.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.infrontfinance.hackathon.iodevices.cfgservice.client.api.ConfigApi;
import feign.Client;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpClientConfiguration {
  @Bean
  public ConfigApi configApi(final Client client, final ObjectMapper objectMapper) {
    return Feign.builder()
        .client(client)
        .decoder(new JacksonDecoder(objectMapper))
        .encoder(new JacksonEncoder(objectMapper))
        .target(ConfigApi.class, "http://35.192.68.100");
  }
}
