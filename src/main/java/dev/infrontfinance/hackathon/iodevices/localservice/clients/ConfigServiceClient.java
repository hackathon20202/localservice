package dev.infrontfinance.hackathon.iodevices.localservice.clients;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.api.ConfigApi;
import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Config;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ConfigServiceClient {

  private final ConfigApi configApi;

  public Config getConfig(UUID configID) {
    return configApi.getConfigWithID(configID);
  }
}
