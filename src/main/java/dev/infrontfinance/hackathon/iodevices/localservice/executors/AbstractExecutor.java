package dev.infrontfinance.hackathon.iodevices.localservice.executors;

import dev.infrontfinance.hackathon.iodevices.cfgservice.client.model.Action;

public interface AbstractExecutor {

  void execute(Action action);
}
