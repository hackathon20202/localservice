package dev.infrontfinance.hackathon.iodevices.localservice.web.api;

import dev.infrontfinance.hackathon.iodevices.localservice.api.DeviceApiApi;
import dev.infrontfinance.hackathon.iodevices.localservice.controller.LocalServiceController;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class LocalServiceRestController implements DeviceApiApi {

  private final LocalServiceController localServiceController;

  /**
   * PUT /configs/{configID} execute an specific action
   *
   * @param configID The config id you want to interact with (required)
   * @return Executed (status code 204)
   */
  @Override
  public ResponseEntity<Void> executeAction(UUID configID) {
    log.info("executeAction: " + configID.toString());
    localServiceController.executeConfig(configID);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
