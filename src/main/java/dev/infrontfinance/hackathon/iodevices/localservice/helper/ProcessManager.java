package dev.infrontfinance.hackathon.iodevices.localservice.helper;

import com.michaelalynmiller.jnaplatext.win32.ProcessUtils;
import com.sun.jna.platform.DesktopWindow;
import com.sun.jna.platform.win32.WinDef.HWND;
import dev.infrontfinance.hackathon.iodevices.localservice.exception.LocalServiceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ProcessManager {

  public static Optional<DesktopWindow> getProcessWindow(final int processId) throws Exception {
    List<HWND> processList = ProcessUtils.getProcessWindows(processId);
    List<DesktopWindow> visibleWindows = TaskManager.getVisibleWindows(processList);
    if (visibleWindows.isEmpty()) {
      return Optional.empty();
    } else if (1 < visibleWindows.size()) {
      throw new LocalServiceException(
          "More than 1 window open for process: " + visibleWindows.get(0).getFilePath());
    } else {
      return Optional.of(visibleWindows.get(0));
    }
  }

  public static int getProcessId(String processName) throws IOException {
    Optional<TaskListEntry> taskListEntry = getTaskListEntry(processName);
    if (taskListEntry.isPresent()) {
      return taskListEntry.get().pid;
    }
    throw new LocalServiceException("No process id found for " + processName);
  }

  public static Optional<TaskListEntry> getTaskListEntry(String processName) throws IOException {
    ArrayList<TaskListEntry> taskListEntries = new ArrayList<>();
    ProcessBuilder processBuilder = new ProcessBuilder("tasklist.exe");
    Process process = processBuilder.start();
    List<String> lines = getAllLines(process);
    String[] seperators = lines.get(2).split(" ");
    if (seperators.length < 5) {
      throw new IllegalStateException("Incorrect number of seperators parsed");
    }
    lines = lines.subList(4, lines.size() - 1); // remove header
    lines.forEach(s -> taskListEntries.add(new TaskListEntry(seperators, s)));
    process.destroy();
    if (process.isAlive()) {
      process.destroyForcibly();
    }
    return taskListEntries.stream()
        .filter(taskListEntry -> taskListEntry.image.equals(processName))
        .findFirst();
  }

  private static List<String> getAllLines(Process process) throws IOException {
    BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
    List<String> lines = new ArrayList<>();
    String line;
    while ((line = input.readLine()) != null) {
      lines.add(line);
    }
    input.close();
    return lines;
  }

  public static boolean isProcessRunning(String processName) throws IOException {
    return getTaskListEntry(processName).isPresent();
  }

  @Data
  public static class TaskListEntry {
    String image;
    int pid;
    String sessionName;
    int sessionNum;
    String memUsage;

    private TaskListEntry(String[] seperators, String line) {
      try {
        int start = 0;
        int end = start + seperators[0].length() + 1;
        this.image = line.substring(start, end).trim();

        start = end;
        end = start + seperators[1].length() + 1;
        this.pid = Integer.parseInt(line.substring(start, end).trim());

        start = end;
        end = start + seperators[2].length() + 1;
        this.sessionName = line.substring(start, end).trim();

        start = end;
        end = start + seperators[3].length() + 1;
        this.sessionNum = Integer.parseInt(line.substring(start, end).trim());

        start = end;
        end = start + seperators[4].length();
        this.memUsage = line.substring(start, end).trim();
      } catch (NullPointerException e) {
        throw e;
      } catch (RuntimeException e) {
        String format = "Unable to parse line {%s} with seperator {%s}";
        String msg = String.format(format, line, Arrays.toString(seperators));
        throw new IllegalArgumentException(msg, e);
      }
    }
  }
}
