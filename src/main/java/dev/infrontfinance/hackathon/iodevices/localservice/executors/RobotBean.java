package dev.infrontfinance.hackathon.iodevices.localservice.executors;

import java.awt.AWTException;
import java.awt.Robot;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RobotBean {
  @Bean
  Robot robot() throws AWTException {
    return new Robot();
  }
}
