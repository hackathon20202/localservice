package dev.infrontfinance.hackathon.iodevices.localservice;

import java.util.Collections;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * This is a helper Java class that provides an alternative to creating a web.xml. This will be invoked only when the application is deployed to a servlet container like Tomcat, JBoss etc.
 */
public class ApplicationWebXml extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    /* set a default to use when no profile is configured. */
    application.application().setDefaultProperties(Collections.singletonMap(
        "spring.profiles.default", "dev"
    ));
    return application.sources(LocalServiceApplication.class);
  }
}
